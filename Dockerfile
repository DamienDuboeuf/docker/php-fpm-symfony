ARG PHP_VERSION=7.4-fpm

FROM registry.gitlab.com/damienduboeuf/docker/php-xdebug-disable:${PHP_VERSION}

ARG PKG=

MAINTAINER Damien DUBOEUF <duboeuf.damien@gmail.com>

RUN apk add --no-cache

RUN install-php-extensions \
        pdo_mysql \
        pdo_pgsql \
        mbstring \
        json \
        intl \
        zip \
        opcache \
        apcu \
        gd \
        imagick \
        apcu
